import React from "react";
import { View, Button, Text, StyleSheet, TextInput } from "react-native";
import MovieList from "../movieList";
import {fetchMovies} from '../api'

export default class SearchScreen extends React.Component {
  
  static navigationOptions = ({navigation}) => {
    return {
      headerTitle: "Browse Movies"
    }
  }
  
  state = {
    searchString: "",
    movies: [],
    searchPages: 1,
  }
  
  addToMovies = newMovies => {
    this.setState(
      prevState => ({
        movies: prevState.movies.concat(newMovies)
      })
    )
  }

  handleTextChange = searchString => {
    searchString = searchString.trim()
    this.setState({searchString}, () => this.createMovieList())
  }

  //initial movie list taking in 1 page
  createMovieList = async () => {
    const movies = await fetchMovies(this.state.searchString, 1)
    this.setState({movies})
  }

  addPage = async () => {
    this.setState(prevState => ({searchPages: prevState.searchPages + 1 }), () => this.updateMovieList(this.state.searchPages))
  }

  updateMovieList = async (page) => {
    const moviesToAdd = await fetchMovies(this.state.searchString, page)
    this.addToMovies(moviesToAdd)
  }

  render() {
    return (
      <View style={styles.container}>
          <TextInput
            style={styles.searchBar}
            value={this.state.search}
            onChangeText={this.handleTextChange}
            placeholder="Search..."
          />
          <MovieList
            data={this.state.movies}
            onSelectMovie={() => this.props.navigation.navigate("Info")}
            addPage={this.addPage}
          />
        </View>
    )
  }
}

const styles = StyleSheet.create({
  searchScreenContainer: {
    flex: 1,
    alignItems: "center"
  },
  searchBar: {
      margin: 20,
      padding: 10,
      borderColor: 'black',
      borderWidth: 1,
      width: "90%",
      alignSelf: "center",
  }
});
