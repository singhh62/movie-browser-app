const end_point="http://www.omdbapi.com/?apikey=fd448c3&"

const processedResult = (movie) => ({
    title: movie.Title,
    image: {uri: movie.Poster},
    year : movie.Year,
    id   : movie.imdbID,
    type : movie.Type
})

export const fetchMovies = async (keyword, page) => {
  const keyword_search_endpoint = end_point + 's=' + keyword + '&page=' + page
  const response = await fetch(keyword_search_endpoint).then(response => response.json()).catch((error) => console.log('There has been a problem with your fetch operation: ' + error.message))
  if (response.Response == "True") {
    return response.Search.map(processedResult)
  }
  else{
    return []
  }
}
