import React from 'react'
import {SafeAreaView, FlatList, StyleSheet} from 'react-native'
import MovieRow from './movieRow'

const MovieList = (props) => {

    const renderTheItem = ({item}) => {
        return (
            <MovieRow {... item} onSelectMovie={props.onSelectMovie}/>
        )
    }

    const extractTheKey = (movieItem) => movieItem.id

    return (
        <FlatList
            data={props.data}
            renderItem={renderTheItem}
            keyExtractor={extractTheKey}
            onEndReachedThreshold={0.3}
            onEndReached={() => props.addPage()}
        />
    )
}

export default MovieList