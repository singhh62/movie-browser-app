import {Text, TouchableOpacity, View,Image, StyleSheet} from 'react-native'
import React from 'react'

const styles = StyleSheet.create({
    row: {
        flexDirection: 'row',
        height: 80,
        marginTop: 5,
        marginBottom: 5

    },
    image:{
      height: 80,
      width: 80,
      alignSelf: "flex-start",
      
    },
    content:{
      alignSelf: "center",
      paddingLeft: 30,
      padding: 5
    }
})

export default props => (
  <TouchableOpacity style={styles.row} onPress={() => props.onSelectMovie(props)}>
    <Image style={styles.image} source={props.image}/>
      <View  style={styles.content}>
        <Text style={{fontWeight: 'bold'}}>{props.title}</Text>
        <Text>{props.year + ' (' + props.type+ ')'}</Text>
      </View>
  </TouchableOpacity>
)