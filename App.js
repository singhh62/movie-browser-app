import React from "react";
import { StyleSheet } from "react-native";
import { createAppContainer } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";
import InfoScreen from "./screens/infoScreen";
import SearchScreen from "./screens/searchScreen";

const AppNavigator = createStackNavigator(
  {
    Info: InfoScreen,
    Search: SearchScreen
  },
  { initialRouteName: "Search" }
);

const AppContainer = createAppContainer(AppNavigator);

export default class App extends React.Component {

  render() {
    return <AppContainer/>;
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  }
});